# Home work app 

Simple app for finding country by phone number.

## To run app:  
### With maven installed:  
```
mvn clean install spring-boot:run
```
### Maven wrapper:  
```
./mvnw clean install spring-boot:run
```

or for windows  
```
mvnw.cmd clean install spring-boot:run
```

## To run test and generate reports:
```
mvn clean test jacoco:report
```

Reports can be found at: `target/site/jacoco/index.html`

## Urls: 
After app start you can check index page at: (http://localhost:8080/). 

Rest documentation can be found at (http://localhost:8080/docs.html).