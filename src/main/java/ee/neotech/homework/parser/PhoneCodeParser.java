package ee.neotech.homework.parser;

import java.util.List;

public interface PhoneCodeParser {
    List<PhoneCodes> parsePhoneCodeFromExternalSource();
}
