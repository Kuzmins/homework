package ee.neotech.homework.parser;

import lombok.Data;

import java.util.List;

@Data
public class PhoneCodes {
    private final List<String> codes;
    private final String country;
}
