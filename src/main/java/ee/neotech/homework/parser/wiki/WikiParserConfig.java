package ee.neotech.homework.parser.wiki;

import lombok.Data;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@ConfigurationProperties(prefix = "parser")
@ConstructorBinding
@Validated
@Data
public class WikiParserConfig {
    @URL
    @NotBlank
    private final String url;

    public String getUrl() {
        return url;
    }
}
