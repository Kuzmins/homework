package ee.neotech.homework.parser.wiki;

import com.google.common.base.CharMatcher;
import ee.neotech.homework.parser.PhoneCodeParser;
import ee.neotech.homework.parser.PhoneCodes;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkState;


@Service
public class WikipediaPhoneCodeParser implements PhoneCodeParser {

    private final RestTemplate restTemplate;
    private final WikiParserConfig config;

    public WikipediaPhoneCodeParser(RestTemplate restTemplate, WikiParserConfig config) {
        this.restTemplate = restTemplate;
        this.config = config;
    }

    @Override
    public List<PhoneCodes> parsePhoneCodeFromExternalSource() {
        Optional<String> htmlOptional = Optional.ofNullable(restTemplate.getForObject(config.getUrl(), String.class));
        return htmlOptional.map(html -> {
            Document doc = Jsoup.parse(html);
            Element tableHeader = doc.getElementById("Alphabetical_listing_by_country_or_region").parent();
            Element table = tableHeader.nextElementSibling();
            Elements rows = table.select("tbody > tr");
            List<PhoneCodes> codes = rows.stream().skip(1).map(this::extractPhoneCodesFromRow).collect(Collectors.toList());
            checkState(!codes.isEmpty());
            return codes;
        }).orElseThrow(() -> new IllegalStateException("Page is empty!"));
    }

    private PhoneCodes extractPhoneCodesFromRow(Element tableRow) {
        Elements columns = tableRow.select("td");
        List<String> phoneCodes = columns.get(1).select("span + a").stream()
                .map(this::extractDigitsFromString)
                .collect(Collectors.toList());

        return new PhoneCodes(
                phoneCodes,
                columns.get(0).text()
        );
    }

    private String extractDigitsFromString(Element phoneCodeElement) {
        return CharMatcher.inRange('0', '9').retainFrom(phoneCodeElement.text());
    }
}
