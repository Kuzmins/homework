package ee.neotech.homework.rest;

import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class ApiError {
    private final String message;
    private final List<FieldError> fieldErrors;
    private final List<String> globalErrors;
    private final Instant time = Instant.now();

    @Data
    public static class FieldError {
        private final String field;
        private final String error;
        private final String code;
    }
}
