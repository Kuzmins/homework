package ee.neotech.homework.rest;

import lombok.Data;

import java.util.List;

@Data
public class FoundedPhoneCodeResponse {
    private final String phoneNumber;
    private final String code;
    private final List<String> countries;
}
