package ee.neotech.homework.rest;

import com.google.common.base.Preconditions;
import ee.neotech.homework.search.PhoneSearchService;
import ee.neotech.homework.search.FoundedPhoneCode;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class PhoneSearchController {

    private final PhoneSearchService phoneSearchService;

    @PostMapping("/rest/phone/info")
    public FoundedPhoneCodeResponse findCountryByCode(@Validated @RequestBody SearchCountryRequest request) {
        String phoneNumber = request.getPhoneNumber();
        Preconditions.checkArgument(StringUtils.isNumeric(phoneNumber), "Phone number should contain only numbers!");
        FoundedPhoneCode foundedCode = phoneSearchService.findCountryByPhoneNumber(phoneNumber);
        return new FoundedPhoneCodeResponse(foundedCode.getPhoneNumber(), foundedCode.getPhoneCode(), foundedCode.getCountries());
    }
}
