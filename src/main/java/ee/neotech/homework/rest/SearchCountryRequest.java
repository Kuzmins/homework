package ee.neotech.homework.rest;

import javax.validation.constraints.Size;

public class SearchCountryRequest {
    @Size(min = 5, max = 30)
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public SearchCountryRequest setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
