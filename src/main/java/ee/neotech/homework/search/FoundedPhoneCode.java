package ee.neotech.homework.search;

import lombok.Data;

import java.util.List;

@Data
public class FoundedPhoneCode {
    private final String phoneNumber;
    private final String phoneCode;
    private final List<String> countries;
}
