package ee.neotech.homework.search;

import com.google.common.base.Preconditions;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PhoneCodeSearchTree {

    private final PhoneCodeNode[] firstDigitNodes = new PhoneCodeNode[10];

    public void addPhoneCodeWithCountry(String phoneCode, String country) {
        Preconditions.checkArgument(StringUtils.isNumeric(phoneCode), "Should pass only numeric code!");
        int firstDigit = Character.getNumericValue(phoneCode.charAt(0));
        PhoneCodeNode phoneCodeNode = firstDigitNodes[firstDigit];
        if (phoneCodeNode == null) {
            phoneCodeNode = new PhoneCodeNode();
            phoneCodeNode.depth = 1;
            firstDigitNodes[firstDigit] = phoneCodeNode;
        }
        phoneCodeNode.assignDigitToCountry(phoneCode, country);
    }

    public FoundedPhoneCode findCountryByPhoneNumber(String phoneNumber) {
        Preconditions.checkArgument(StringUtils.isNumeric(phoneNumber), "Should pass only numeric code!");
        int firstDigit = Character.getNumericValue(phoneNumber.charAt(0));

        return Optional.ofNullable(firstDigitNodes[firstDigit])
                .map(node -> node.findCountryByDigit(phoneNumber))
                .map(searchResult -> new FoundedPhoneCode(phoneNumber, phoneNumber.substring(0, searchResult.depth), searchResult.countries))
                .orElse(new FoundedPhoneCode(phoneNumber, null, Collections.emptyList()));

    }

    private static class PhoneCodeNode {
        private Integer depth;
        private final List<String> countries = new ArrayList<>();
        private final PhoneCodeNode[] children = new PhoneCodeNode[10];

        private void assignDigitToCountry(String digits, String country) {
            if (digits.length() == 1) {
                this.countries.add(country);
            } else {
                String remainDigits = digits.substring(1);
                assignNextDigitsToChildNode(remainDigits, country);
            }
        }

        private void assignNextDigitsToChildNode(String remainDigits, String country) {
            int firstRemainDigit = Character.getNumericValue(remainDigits.charAt(0));
            PhoneCodeNode childNodeForDigit = children[firstRemainDigit];
            if (childNodeForDigit == null) {
                childNodeForDigit = new PhoneCodeNode();
                childNodeForDigit.depth = this.depth + 1;
                children[firstRemainDigit] = childNodeForDigit;
            }
            childNodeForDigit.assignDigitToCountry(remainDigits, country);
        }

        private SearchResult findCountryByDigit(String remainDigits) {
            // We start search with second digit if not found then current country is match
            int nextRemainDigit = Character.getNumericValue(remainDigits.charAt(1));
            return Optional.ofNullable(children[nextRemainDigit])
                    .map(childNode -> childNode.findCountryByDigit(remainDigits.substring(1)))
                    .orElse(getCurrentResultOrNull());
        }

        private SearchResult getCurrentResultOrNull() {
            if (this.countries.isEmpty()) {
                return null;
            }
            return new SearchResult(this.depth, this.countries);
        }
    }

    @Data
    private static class SearchResult {
        private final Integer depth;
        private final  List<String> countries;
    }
}
