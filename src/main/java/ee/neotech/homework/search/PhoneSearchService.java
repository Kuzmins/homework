package ee.neotech.homework.search;

public interface PhoneSearchService {
    FoundedPhoneCode findCountryByPhoneNumber(String phoneNumber);
}
