package ee.neotech.homework.search;

import ee.neotech.homework.parser.PhoneCodeParser;
import ee.neotech.homework.parser.PhoneCodes;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Stream;

@Service
public class PhoneSearchServiceImpl implements PhoneSearchService, InitializingBean {

    private final PhoneCodeParser parser;
    private final PhoneCodeSearchTree searchTree;

    public PhoneSearchServiceImpl(PhoneCodeParser parser, PhoneCodeSearchTree searchTree) {
        this.parser = parser;
        this.searchTree = searchTree;
    }

    @Override
    public FoundedPhoneCode findCountryByPhoneNumber(String testPhoneNumber) {
        return searchTree.findCountryByPhoneNumber(testPhoneNumber);
    }

    @Override
    public void afterPropertiesSet() {
        List<PhoneCodes> phoneCodes = parser.parsePhoneCodeFromExternalSource();
        phoneCodes.stream()
                .flatMap(this::mapToSinglePhoneCode)
                .forEach(singleCode -> searchTree.addPhoneCodeWithCountry(singleCode.phoneCode, singleCode.country));
    }

    private Stream<SinglePhoneCode> mapToSinglePhoneCode(PhoneCodes phoneCodes) {
       return  phoneCodes.getCodes().stream()
               .map(code -> new SinglePhoneCode(code, phoneCodes.getCountry()));
    }

    @Data
    private static class SinglePhoneCode {
        private final String phoneCode;
        private final String country;
    }
}
