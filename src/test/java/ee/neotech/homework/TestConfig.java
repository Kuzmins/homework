package ee.neotech.homework;

import ee.neotech.homework.parser.PhoneCodeParser;
import ee.neotech.homework.parser.PhoneCodes;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;

import java.util.Arrays;
import java.util.Collections;

@TestConfiguration
public class TestConfig {

    @Bean
    @Primary
    public PhoneCodeParser mockedPhoneCodeParser() {
        return () -> Arrays.asList(
                new PhoneCodes(Collections.singletonList("371"), "Latvia"),
                new PhoneCodes(Collections.singletonList("372"), "Lithuania")
        );
    }
}
