package ee.neotech.homework.parser;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import ee.neotech.homework.parser.wiki.WikiParserConfig;
import ee.neotech.homework.parser.wiki.WikipediaPhoneCodeParser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

class WikipediaPhoneCodesParserTest {

    WireMockServer wireMockServer;

    WikipediaPhoneCodeParser parser;

    @BeforeEach
    void setUp() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        WireMock.configureFor(wireMockServer.port());
        String wikiMockedUrl = "http://localhost:" + wireMockServer.port();
        parser = new WikipediaPhoneCodeParser(new RestTemplate(), new WikiParserConfig(wikiMockedUrl));
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }

    @Test
    void shouldParseWikiGetPhoneNumbers() throws IOException {
        // Given: wiki page with phone codes available
        String html = readResource("stubs/List_of_country_calling_codes.html", Charsets.UTF_8);
        stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse()
                        .withBody(html)));

        // When: parsing phone codes
        List<PhoneCodes> phoneCodes = parser.parsePhoneCodeFromExternalSource();


        // Then: phone code is parsed
        assertThat(phoneCodes, hasSize(280));

        // And: countries with multiple also parsed
        PhoneCodes dominicanPhoneCodes = phoneCodes.stream()
                .filter(phoneCode -> "Dominican Republic".equals(phoneCode.getCountry()))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("no code found"));

        assertThat(dominicanPhoneCodes.getCodes(), hasSize(3));
        assertThat(dominicanPhoneCodes.getCodes().get(0), is("1809"));
    }

    @Test
    void shouldThrowExceptionIfCanNotReachWiki() {
        // Given: wiki page can not be reached
        stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse()
                        .withFault(Fault.EMPTY_RESPONSE)));

        // When: trying to parse wiki, then exception is thrown
        Assertions.assertThrows(RestClientException.class, () -> {
            parser.parsePhoneCodeFromExternalSource();
        });
    }

    @Test
    void shouldThrowIfNoPhoneCodesAreParsed() throws IOException {
        // Given: wiki page has empty table
        String html = readResource("stubs/Wiki_page_with_empty_table.html", Charsets.UTF_8);

        stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse()
                        .withBody(html)));

        // When: trying to parse wiki, then exception is thrown
        Assertions.assertThrows(IllegalStateException.class, () -> {
            parser.parsePhoneCodeFromExternalSource();
        });
    }

    @Test
    void shouldThrowErrorIfHtmlPageIsEmpty() {
        // Given: wiki page has empty table
        stubFor(get(urlEqualTo("/"))
                .willReturn(aResponse()));

        // When: trying to parse wiki, then exception is thrown
        IllegalStateException exception = Assertions.assertThrows(IllegalStateException.class, () -> {
            parser.parsePhoneCodeFromExternalSource();
        });
        assertThat(exception.getMessage(), is("Page is empty!"));
    }

    private String readResource(final String fileName, Charset charset) throws IOException {
        return Resources.toString(Resources.getResource(fileName), charset);
    }
}