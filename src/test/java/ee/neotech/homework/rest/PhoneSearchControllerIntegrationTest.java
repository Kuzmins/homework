package ee.neotech.homework.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.neotech.homework.BaseIntegrationTest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PhoneSearchControllerIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldFindCountryByPhoneNumber() throws Exception {

        this.mockMvc.perform(post("/rest/phone/info")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new SearchCountryRequest().setPhoneNumber("37122888086"))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.countries[0]").value("Latvia"))
                .andExpect(jsonPath("$.code").value("371"))
                .andDo(document("phone-search"));
    }

    @Test
    public void shouldReturnBadRequestIfExceptionThrown() throws Exception {
        this.mockMvc.perform(post("/rest/phone/info")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new SearchCountryRequest().setPhoneNumber("someNonNumeric"))))

                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(0)))
                .andExpect(jsonPath("$.globalErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.globalErrors[0]").value("Phone number should contain only numbers!"))
                .andDo(document("phone-search-global-errors"));
    }

    @Test
    public void shouldReturnBadRequestIfValidationFail() throws Exception {
        this.mockMvc.perform(post("/rest/phone/info")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new SearchCountryRequest().setPhoneNumber("235"))))

                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.globalErrors", Matchers.hasSize(0)))
                .andExpect(jsonPath("$.fieldErrors", Matchers.hasSize(1)))
                .andExpect(jsonPath("$.fieldErrors[0].field").value("phoneNumber"))
                .andExpect(jsonPath("$.fieldErrors[0].error").value("size must be between 5 and 30"))
                .andExpect(jsonPath("$.fieldErrors[0].code").value("Size"))
                .andDo(document("phone-search-field-errors"));
    }
}