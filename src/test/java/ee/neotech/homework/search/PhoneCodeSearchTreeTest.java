package ee.neotech.homework.search;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.fail;

class PhoneCodeSearchTreeTest {

    private final PhoneCodeSearchTree searchTree = new PhoneCodeSearchTree();

    @Test
    void shouldNotAllowToAddNonNumericCode() {
        // Given:
        String wrongNonNumericNumber = "something";

        // When:
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            searchTree.addPhoneCodeWithCountry(wrongNonNumericNumber, "country");
        });

        // Then:
        assertThat(exception.getMessage(), is("Should pass only numeric code!"));
    }

    @Test
    void shouldFoundAddedCodeByFirstDigitMatch() {
        // Given:
        searchTree.addPhoneCodeWithCountry("1", "USA");

        // When: Pass code which start as USA code
        FoundedPhoneCode phoneCode = searchTree.findCountryByPhoneNumber("13032450086");

        // Then:
        assertThat(phoneCode.getCountries().isEmpty(), is(false));
        assertThat(phoneCode.getCountries().get(0), is("USA"));
    }

    @Test
    void shouldFoundAddedCodeIfFirstDigitNotAssignToCountryButSecondOneAssigned() {
        // Given:
        searchTree.addPhoneCodeWithCountry("1", "USA");
        searchTree.addPhoneCodeWithCountry("20", "Egypt");

        // When:
        FoundedPhoneCode phoneCode = searchTree.findCountryByPhoneNumber("2034223");

        // Then:
        assertThat(phoneCode.getCountries().isEmpty(), is(false));
        assertThat(phoneCode.getPhoneCode(), is("20"));
        assertThat(phoneCode.getCountries().get(0), is("Egypt"));
    }

    @Test
    void shouldReturnNextMatchCountryIfHasMorePreciseCode() {
        // Given:
        searchTree.addPhoneCodeWithCountry("1", "USA");
        searchTree.addPhoneCodeWithCountry("1242", "Bahamas");

        // When:
        FoundedPhoneCode phoneCode = searchTree.findCountryByPhoneNumber("12425555523");

        // Then:
        assertThat(phoneCode.getCountries().isEmpty(), is(false));
        assertThat(phoneCode.getCountries().get(0), is("Bahamas"));
        assertThat(phoneCode.getPhoneCode(), is("1242"));
    }

    @Test
    void shouldNotReturnAnyCountryIfCodeNotFound() {
        // Given:
        searchTree.addPhoneCodeWithCountry("371", "Latvia");

        // When: pass phone with mistake
        FoundedPhoneCode phoneCode = searchTree.findCountryByPhoneNumber("3732288086");

        // Then:
        assertThat(phoneCode.getCountries().isEmpty(), is(true));

        // When: correcting mistake
        phoneCode = searchTree.findCountryByPhoneNumber("3712288086");

        // Then: phoneCode found
        assertThat(phoneCode.getCountries().isEmpty(), is(false));
        assertThat(phoneCode.getCountries().get(0), is("Latvia"));
        assertThat(phoneCode.getPhoneCode(), is("371"));
    }

    @Test
    void shouldReturnMultipleCountriesIfFound() {
        // Given:
        searchTree.addPhoneCodeWithCountry("1", "USA");
        searchTree.addPhoneCodeWithCountry("1", "Canada");

        // When:
        FoundedPhoneCode phoneCode = searchTree.findCountryByPhoneNumber("1324234");

        // Then: matched phoneCode found
        assertThat(phoneCode.getCountries(), hasSize(2));
        assertThat(phoneCode.getCountries().get(0), is("USA"));
        assertThat(phoneCode.getCountries().get(1), is("Canada"));
    }

    @Test
    void shouldReturnUpperCountryIfPhoneCodeNotEnoughPreciseToMatchNextNode() {
        // Given:
        searchTree.addPhoneCodeWithCountry("1", "USA");
        searchTree.addPhoneCodeWithCountry("1340", "US Virgin Islands");

        // When:
        FoundedPhoneCode phoneCode = searchTree.findCountryByPhoneNumber("1341234");

        // Then:
        assertThat(phoneCode.getCountries().isEmpty(), is(false));
        assertThat(phoneCode.getCountries().get(0), is("USA"));
        assertThat(phoneCode.getPhoneCode(), is("1"));
    }
}