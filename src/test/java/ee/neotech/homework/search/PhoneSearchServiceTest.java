package ee.neotech.homework.search;

import ee.neotech.homework.parser.PhoneCodeParser;
import ee.neotech.homework.parser.PhoneCodes;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

class PhoneSearchServiceTest {

    private PhoneCodeParser phoneCodeParser = mock(PhoneCodeParser.class);
    private PhoneCodeSearchTree phoneCodeSearchTree = spy(new PhoneCodeSearchTree());

    PhoneSearchServiceImpl searchService = new PhoneSearchServiceImpl(phoneCodeParser, phoneCodeSearchTree);

    @Test
    void shouldGetPhoneCodesFromParserAndLoadIntoSearchTree() {
        // Given: parser return some codes
        PhoneCodes dominicaCodes = new PhoneCodes(Arrays.asList("1809", "1829"), "Dominican Republic");
        PhoneCodes congoCode = new PhoneCodes(Collections.singletonList("242"), "Congo");
        List<PhoneCodes> someTestCodes = Arrays.asList(dominicaCodes, congoCode);
        when(phoneCodeParser.parsePhoneCodeFromExternalSource()).thenReturn(someTestCodes);

        // When: all related beans initialized
        searchService.afterPropertiesSet();

        // Then: phone codes loaded into tree
        verify(phoneCodeSearchTree).addPhoneCodeWithCountry("1809", "Dominican Republic");
        verify(phoneCodeSearchTree).addPhoneCodeWithCountry("1829", "Dominican Republic");
        verify(phoneCodeSearchTree).addPhoneCodeWithCountry("242", "Congo");
    }

    @Test
    void shouldSearchForCodeInTree() {
        // Given: codes loaded into tree
        PhoneCodes congoCode = new PhoneCodes(Collections.singletonList("242"), "Congo");
        when(phoneCodeParser.parsePhoneCodeFromExternalSource()).thenReturn(Collections.singletonList(congoCode));
        searchService.afterPropertiesSet();

        // When: service is used for country search
        String testPhoneNumber = "24288878234";
        FoundedPhoneCode phoneCode = searchService.findCountryByPhoneNumber(testPhoneNumber);

        // Then: searchTree is used and result returned
        verify(phoneCodeSearchTree).findCountryByPhoneNumber(testPhoneNumber);
        assertThat(phoneCode.getCountries(), hasSize(1));
        assertThat(phoneCode.getCountries().get(0), is("Congo"));
    }
}